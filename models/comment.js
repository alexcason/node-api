var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema(
{
	text: String,
	created: Date,
	post: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
	user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
},
{
	toJSON: {
		virtuals: true,
		transform: function(doc, ret, options) {
			console.log('Comment JSON transform');
			// sub-document transform workaround
			if (doc.schema.options.toJSON && doc.schema.options.toJSON.transform === arguments.callee) { 
		      	// if matches, do the transform for this doc type
		      	delete ret._id;
				delete ret.__v;
				delete ret.post;
		    } else if (doc.schema.options.toJSON && doc.schema.options.toJSON.transform !== arguments.callee) {
		      	// if not, call the transform for the doc we're actually transforming
		      	doc.schema.options.toJSON.transform(doc, ret, options);
		    }
		}
	}
});

module.exports = mongoose.model('Comment', commentSchema);