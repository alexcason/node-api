var mongoose = require('mongoose');

// sub-document transform bug: https://github.com/LearnBoost/mongoose/issues/1412

var postSchema = new mongoose.Schema(
{
	title: String,
	text: String,
	url: String,
	created: Date,
	user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
},
{
	toJSON: {
		virtuals: true,
		transform: function(doc, ret, options) {
			// sub-document transform workaround
			if (doc.schema.options.toJSON && doc.schema.options.toJSON.transform === arguments.callee) { 
		      	// if matches, do the transform for this doc type
		      	delete ret._id;
				delete ret.__v;
		    } else if (doc.schema.options.toJSON && doc.schema.options.toJSON.transform !== arguments.callee) {
		      	// if not, call the transform for the doc we're actually transforming
		      	doc.schema.options.toJSON.transform(doc, ret, options);
		    }
		}
	}
});

module.exports = mongoose.model('Post', postSchema);