var mongoose = require('mongoose');

var userSchema = new mongoose.Schema(
{
	email: String,
	password: String,
	firstName: String,
	lastName: String,
	created: Date,
	posts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
	comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
},
{
	toJSON: {
		virtuals: true,
		transform: function(doc, ret, options) {
			// sub-document transform workaround
			if (doc.schema.options.toJSON && doc.schema.options.toJSON.transform === arguments.callee) { 
		      	// if matches, do the transform for this doc type
		      	delete ret._id;
				delete ret.__v;
				delete ret.password;
				delete ret.firstName;
				delete ret.lastName;
		    } else if (doc.schema.options.toJSON && doc.schema.options.toJSON.transform !== arguments.callee) {
		      	// if not, call the transform for the doc we're actually transforming
		      	doc.schema.options.toJSON.transform(doc, ret, options);
		    }
		}
	}
});

// virtuals

userSchema.virtual('fullName').get(function() {
	return this.firstName + ' ' + this.lastName;
});

// methods

userSchema.methods.validPassword = function(password) {
	return this.password == password;
}

module.exports = mongoose.model('User', userSchema);