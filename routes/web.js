var mongoose = require('mongoose'),
	passport = require('passport');

var utilities = require('../utilities/utilities');
var auth = require('../utilities/auth');

var Post = require('../models/post');
var Comment = require('../models/comment');
var User = require('../models/user');

exports.showPosts = function(req, res) {
	Post
	.find()
	.populate('_user')
	.exec(function(err, posts) {
		if (err) return utilities.handleError(err, 'Unable to find post');

		if (posts != null) {
			res.render('index', {
				posts: posts
			});
		}
	});
}

exports.createPost = function(req, res) {
	// TODO: Replace hard coded user id
	User.findOne({ _id: '51571d33b6a6d23e0bc80373' }, function(err, user) {
		if (err) return utilities.handleError(err, 'Unable to find user');

		var post = new Post({
			title: req.body.title,
			text: req.body.text,
			url: req.body.url,
			created: new Date(),
			_user: user._id
		});

		post.save(function(err) {
			if (err) return utilities.handleError(err, 'Unable to save post');

			user.posts.push(post._id);

			res.render({ 'post': post });
		});
	});
}

exports.showPost = function(req, res) {
	Post
	.findOne({ _id: req.params.id })
	.populate('_user comments')
	.exec(function(err, post) {
		if (err) return utilities.handleError(err, 'Unable to find post');

		if (post != null) {
			console.log(post);
			res.render('post', {
				post: post
			});
		}
	});
}

exports.createComment = function(req, res) {
	// TODO: Replace hard coded user id
	User.findOne({ _id: '51571d33b6a6d23e0bc80373' }, function(err, user) {
		if (err) return utilities.handleError(err, 'Unable to find user');

		Post.findOne({ _id: req.params.id }, function(err, post) {
			if (err) return utilities.handleError(err, 'Unable to find post');

			var comment = new Comment({
				text: req.body.text,
				created: new Date(),
				_post: post._id,
				user: user._id
			});

			comment.save(function(err) {
				if (err) return utilities.handleError(err, 'Unable to save comment');

				user.comments.push(comment._id);
				post.comments.push(comment._id);

				res.render({ 'comment': comment });
			});
		});
	});
}

exports.showUser = function(req, res) {
	User
	.findOne({ _id: req.params.id })
	.populate('posts comments')
	.exec(function(err, user) {
		if (err) return utilities.handleError(err, 'Unable to find user');

		if (user != null) {
			res.render('user', {
				user: user
			});
		}
	});
}

exports.signIn = function(req, res) {
	res.render('signin');
}

exports.signOut = function(req, res) {
	req.logout();
	res.redirect('/');
}

exports.showSignUp = function(req, res) {
	res.render('signup');
}

exports.signUp = function(req, res) {
	console.log('Signup');
}