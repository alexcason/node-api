var mongoose = require('mongoose');

var utilities = require('../utilities/utilities');

var Post = require('../models/post');
var Comment = require('../models/comment');
var User = require('../models/user');

// Object attribute selection: https://github.com/LearnBoost/mongoose/issues/1020

exports.showPosts = function(req, res) {
	Post
	.find()
	.populate('user')
	.exec(function(postError, posts) {
		if (postError) return utilities.handleError(postError, "Error attempting to find post");

		if (posts != null) {
			// respond with found posts
			var postsRes = { 'posts': posts };
			var postsJSON = JSON.stringify(postsRes);

			res.type('json');
			res.send(postsJSON);
		} else {
			// no posts found - respond with Not Found message
			res.send(404);
		}
	});
}

exports.createPost = function(req, res) {
	// TODO: Replace hard coded user id
	User.findOne({ _id: '516c61e9ca22d720fa000001' }, function(userError, user) {
		if (userError) return utilities.handleError(userError, "Error attempting to find user");
		// TODO: check if a user has been found

		// create post document
		var post = new Post({
			title: req.body.title,
			text: req.body.text,
			url: req.body.url,
			created: new Date(),
			user: user._id
		});

		// save post document
		post.save(function(postError) {
			if (postError) return utilities.handleError(postError, "Error attempting to save post");

			// add post to relevant user posts collection
			user.posts.push(post._id);
			user.save();

			// respond with created post
			var postRes = { 'post' : post };
			var postJSON = JSON.stringify(postRes);

			res.type('json');
			res.send(postJSON);
		});
	});
}

exports.showPost = function(req, res) {
	Post
	.findOne({ _id: req.params.id })
	.populate('user comments')
	.exec(function(postError, post) {
		if (postError) return utilities.handleError(postError, "Error attempting to find post");

		if (post != null) {
			// respond with found post
			var postRes = { 'post' : post };
			var postJSON = JSON.stringify(postRes);

			res.type('json');
			res.send(postJSON);
		} else {
			// no post found - respond with Not Found message
			res.send(404);
		}
	});
}

exports.createComment = function(req, res) {
	// TODO: Replace hard coded user id
	User.findOne({ _id: '516c61e9ca22d720fa000001' }, function(userError, user) {
		if (userError) return utilities.handleError(userError, "Error attempting to find user");
		// TODO: check if a user has been found

		// get parent post from id parameter
		Post.findOne({ _id: req.params.id }, function(postError, post) {
			if (postError) return utilities.handleError(postError, "Error attempting to find post");
			// TODO: check if a post has been found

			// create comment document
			var comment = new Comment({
				text: req.body.text,
				created: new Date(),
				post: post._id,
				user: user._id
			});

			// save comment document
			comment.save(function(commentError) {
				if (commentError) return utilities.handleError(commentError, "Error attempting to save comment");

				// add comment to relevant user comments collection
				user.comments.push(comment._id);
				user.save(function(commentUserError) {
					if (commentUserError) return utilities.handleError(commentUserError, "Error attempting to save user");
				});

				// add comment to relevant post comments collection
				post.comments.push(comment._id);
				post.save(function(commentPostError) {
					if (commentPostError) return utilities.handleError(commentPostError, "Error attempting to save post");
				});

				// respond with created comment
				var commentRes = { 'comment': comment};
				var commentJSON = JSON.stringify(commentRes);

				res.type('json');
				res.send(commentJSON);
			});
		});
	});
}

exports.createUser = function(req, res) {
	// create user document
	var user = new User({
		email: req.body.email,
		password: req.body.password,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		created: new Date()
	});

	// save user document
	user.save(function(userError) {
		if (userError) return utilities.handleError(userError, "Error attempting to save user");

		// user created - respond with OK message
		res.send(200);
	});
}

exports.showUser = function(req, res) {
	User
	.findOne({ _id: req.params.id })
	.populate('posts comments')
	.exec(function(userError, user) {
		if (userError) return utilities.handleError(userError, "Error attempting to find user");

		if (user != null) {
			// respond with found user
			var userRes = { 'user': user};
			var userJSON = JSON.stringify(userRes);

			res.type('json');
			res.send(userJSON);
		} else {
			// no user found - respond with Not Found message
			res.send(404);
		}
	});
}