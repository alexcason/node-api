var express = require('express'),
    http = require('http'),
    path = require('path'),
    mongoose = require('mongoose'),
    api = require('./routes/api');

var app = express();

mongoose.connect('mongodb://localhost/smalltalk');

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/posts', api.showPosts);
app.post('/posts', api.createPost);
app.get('/posts/:id', api.showPost);
app.post('/posts/:id/comments', api.createComment);
app.post('/users', api.createUser);
app.get('/users/:id', api.showUser);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
