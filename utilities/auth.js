var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy;

var User = require('../models/user');

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User.findOne({ _id: id }, function(err, user) {
    done(err, user);
  });
});

passport.use(new LocalStrategy ({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(username, password, done) {
    User.findOne({ email: username }, function(err, user) {
      if (err) return done(err);

      if (!user) {
        return done(null, false, { message: 'Incorrect email.' });
      }

      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password' });
      }

      return done(null, user);
    });
  }
));

exports.ensureAuthentication = function(req, res, next) {
	if (req.isAuthenticated()) return next();

	res.redirect('/signin');
}